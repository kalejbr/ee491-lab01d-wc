###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - wc - EE 491 - Spr 2022
###
### @file    Makefile
### @version 2.0
###
### Build a Word Counting program
###
### @author Kale Beever-Riordon <kalejbr@hawaii.edu>
### @date   27_Jan_2022 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c 
	$(CC) $(CFLAGS) -o $(TARGET) wc.c

test: wc
	./wc	tests/README
	wc	tests/README
	./wc tests/test1
	wc	tests/test1
	./wc tests/test2
	wc	tests/test2
	./wc tests/test3
	wc	tests/test3
	./wc tests/test4
	wc	tests/test4
	./wc tests/test5
	wc	tests/test5
	./wc tests/test8
	wc	tests/test8
	./wc tests/test9
	wc	tests/test9
	./wc tests/testa
	wc	tests/testa

clean:
	rm -f $(TARGET)

