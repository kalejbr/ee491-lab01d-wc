/////////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.h
/// @version 1.0 - Initial version
///
///  Header file
///
/// @author  Kale Beever-Riordon <kalejbr@hawaii.edu>
/// @date    27_Jan_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

char** loadfile(char* filename, int cflag, int lflag, int wflag);
static void usage();
bool isaword = 1;
