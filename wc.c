
/////////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 2.0 
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  Kale Beever-Riordon <kalejbr@hawaii.edu>
/// @date    27_Jan_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
////////////////////////////////////////////////////////////////////////////

#include <sys/mman.h>
#include "sys/stat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include "unistd.h"
#include "getopt.h"
#include <limits.h>
#include <locale.h>
#include <fcntl.h>
#include <wchar.h>
#include <wctype.h>
#include "wc.h"

#define STEPSIZE 2
#define SMALLBUFF (8192)

const char name[] = "wc.c";

int main(int argc, char* argv[]) {

    int opt, i;
    double version = 2.0;
    int cflag = 0;
    int lflag = 0;
    int wflag = 0;
    int vflag = 0;
    int o_ind = optind;


    static struct option long_options[] = {
            {"bytes",   required_argument, 0, 'c'},
            {"lines",   required_argument, 0, 'l'},
            {"words",   required_argument, 0, 'w'},
            {"version", no_argument,       0, 'v'},
            {0, 0,                         0, 0}
    };
    int opt_index = 0;
    // loops over the options
    while ((opt = getopt_long(argc, argv, "clwv", long_options, &opt_index)) != -1) {
        //check for a specific char
        switch(opt){
            // short option for bytes
            case 'c':
                cflag++;
                break;
                // short option for lines
            case 'l':
                lflag++;
                break;
                // short option for words
            case 'w':
                wflag++;
                break;
                // short option for version
            case 'v':
                vflag++;
                if (vflag > 0){
                    printf(" %s\t[%.01f]\n", argv[0], version);
                    return ( EXIT_SUCCESS );
                }
                break;
            default:
                  usage();
        }
        argc -= o_ind;
        argv += o_ind;

    }


    if (o_ind < argc)
    {
        for (i = o_ind; i < argc; i++)
        {
           if(!*argv) {
              printf("usage : [%s]\n", argv[i]);
           } else {
            loadfile(argv[i], cflag, lflag, wflag);
           }
        }
    }
    else
    {
        loadfile(NULL, cflag, lflag, wflag);
    }

    return EXIT_SUCCESS;
}
char** loadfile(char* filename, int cflag, int lflag, int wflag)
{
    int countChar=0, countLines=0, countWords=0;
    int fd, len;
    unsigned char *p;
    static unsigned char small_buf[SMALLBUFF];
    static unsigned char *buff = small_buf;
    static off_t buf_size = SMALLBUFF;
    wchar_t wch;
    size_t charLength;

      if( filename == NULL ){
         fd = STDIN_FILENO;
      } else {
         fd = open(filename, O_RDONLY, 0);
      }

      if (fd == -1) {
         printf("wc.c: cannot open [%s]\n", filename);
         return 0;
      }
    
    while((len = read(fd, buff, buf_size)) != 0) {
        p = buff;
        while (len > 0) {
            charLength = 1;
            wch = (unsigned char) *p;
            countChar++;
            len -= (int) charLength;
            p += charLength;
            if (wch == L'\n')
                ++countLines;
            if (iswspace(wch))
                isaword = 1;
            else if (isaword) {
                isaword = 0;
                ++countWords;
            }
        }
    }
       if ( cflag > 0 ){
        printf(" %d bytes", countChar);
    } else if ( lflag > 0 ){
        printf(" %d lines", countLines);
    } else if ( wflag > 0 ){
        printf(" %d words", countWords);
    } else {
        printf("%d\t%d\t%d", countLines, countWords, countChar);
    }
    if(filename != NULL) {
        printf("\t%s\n", filename);
    } else {
        printf("\t%s\n", "from stdin");
    }

    close(fd);

    return EXIT_SUCCESS;
}

static void usage()
{
    (void)fprintf(stderr, "usage: wc [-clwv] [file ...]\n");
    exit ( EXIT_FAILURE );
}
